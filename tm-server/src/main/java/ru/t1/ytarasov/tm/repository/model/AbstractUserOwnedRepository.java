package ru.t1.ytarasov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.api.EntityConstant;
import ru.t1.ytarasov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.ytarasov.tm.model.AbstractUserOwnedModel;
import ru.t1.ytarasov.tm.model.User;

import javax.persistence.EntityManager;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void add(@NotNull final String userId, @NotNull final M model) {
        @NotNull final String jpql = String.format("FROM %s WHERE %s = :id",
                EntityConstant.TABLE_USER, EntityConstant.COLUMN_ID);
        @NotNull final User user = entityManager.find(User.class, userId);
        model.setUser(user);
        entityManager.persist(model);
    }



}
